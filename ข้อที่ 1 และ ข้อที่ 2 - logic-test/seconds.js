// ข้อ 2). //
const monthNames = [
    "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
];
const daysOfWeek = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"];


function dayOfWeek(day, month, year) {
    // Reference https://cs.uwaterloo.ca/~alopez-o/math-faq/node73.html 
    if (month < 3) {
        month += 12;
        year--;
    }
    var dayOfWeek = (day + parseInt(((month + 1) * 26) / 10) +
        year + parseInt(year / 4) + 6 * parseInt(year / 100) +
        parseInt(year / 400) - 1) % 7;
    return daysOfWeek[dayOfWeek];
}

function calDate(date) {
    const dateList = date.split('/')
    const day = parseInt(dateList[0]) ?? 0
    const month = parseInt(dateList[1]) ?? 0
    const year = parseInt(dateList[2]) ?? 0
    if (day === 0 || month === 0 || year === 0) {
        return null
    }
    return `${dayOfWeek(day, month, year)} ${day} ${monthNames[month - 1]} ${year + 543}`.toString()
}
console.log(calDate('06/1/2021'))
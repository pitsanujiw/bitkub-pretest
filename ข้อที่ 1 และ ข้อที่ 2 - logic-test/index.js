// choice 1
function patternOne(n) {
    if (n <= 1) {
        return 'X'
    }
    let string = "";
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < i; j++) {
            string += "o";
        }
        for (let k = 0; k < (n - i) * 2 - 1; k++) {
            string += (k + 1) % 2 ? 'x' : 'o';
        }
        for (let j = 0; j < i; j++) {
            string += "o";
        }
        string += "\n";
    }
    for (let i = 2; i <= n; i++) {
        for (let j = n; j > i; j--) {
            string += "o";
        }
        for (let k = 0; k < i * 2 - 1; k++) {
            string += (k + 1) % 2 ? 'x' : 'o';
        }
        for (let j = n; j > i; j--) {
            string += "o";
        }
        string += "\n";
    }
    return string;
}
// choice 2
function patternTwo(n) {
    if (n <= 1) {
        return 'x'
    }
    let string = "";
    let space = n;
    for (let i = 1; i <= n / 2; i++) {
        space -= 2;
        for (let j = 0; j < i; j++) {
            string += "x";
        }
        for (let j = 0; j < space; j++) {
            string += " ";
        }
        for (let j = 0; j < i; j++) {
            string += "x";
        }
        string += "\n";
    }
    for (let i = 1; i <= n; i++) {
        string += "x";
    }
    string += "\n";
    space = n % 2 == 0 ? n / 2 : n % 2
    for (let i = n / 2; i >= 1; i--) {
        for (let j = 1; j < i; j++) {
            string += "x";
        }
        for (let j = Math.floor(space); j > 0; j--) {
            string += " ";
        }
        for (let j = 1; j < i; j++) {
            string += "x";
        }
        space += 2
        string += "\n";
    }
    return string;
}
// choice 3
function patternThree(n) {
    if (n <= 1) return 'x'
    let string = "";
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            if (n < 3) {
                string += 'X'
            } else if (n === 3) {
                if (i === 1 && j < n - 1) {
                    string += 'Y'
                } else {
                    string += 'X'
                }
            } else {
                if (n === 7 && i === n - 4) {
                    if (j === 1 || j === 2 || j === 3 || j === n - 2) {
                        string += 'Y'
                    } else {
                        string += 'X'
                    }
                } else if (n === 7 && i === n - 3) {
                    if (j === 1 || j === n - 2) {
                        string += 'Y'
                    } else {
                        string += 'X'
                    }
                }
                else
                    if (n === 6 && i === n - 3) {
                        if (j === 1 || j === n - 2) {
                            string += 'Y'
                        } else {
                            string += 'X'
                        }
                    } else if (i === n - 2 && n > 4) {
                        if (j === 0) {
                            string += 'X'
                        } else if (j === n - 1) {
                            string += 'X'
                        } else {
                            string += 'Y'
                        }
                    } else if (i === 2) {
                        if (i === 1 && j < n - 1) {
                            string += 'X'
                        } else if (i === 2 && j === n - 2) {
                            string += 'Y'
                        } else {
                            string += 'X'
                        }
                    }
                    else if (i === 1) {
                        if (j === n - 1) {
                            string += 'X'
                        } else {
                            string += 'Y'
                        }
                    }
                    else {
                        string += 'X'
                    }
            }
        }
        string += "\n";
    }
    return string;
}

// console.log(patternOne(4));
// console.log(patternTwo(5));
// console.log(patternThree(7));


import React from 'react';
import useSWR, { mutate } from 'swr';
import { fetcher, useDebounce } from '../utils';
import {
    TableComponent,
    SearchComponents
} from '../components'

export const TableViewPage = () => {
    const [search, setSearch] = React.useState<string>()
    const { data, error } = useSWR('/users', fetcher)

    const debouncedSearchTerm = useDebounce(search as string, 1000)
    React.useMemo(async () => {
        if (debouncedSearchTerm) {
            const keys = `/users?q=${debouncedSearchTerm}`
            await mutate('/users', fetcher(keys), false);
        } else if (search !== undefined && search?.length === 0) {
            await mutate('/users', fetcher('/users'), false);

        }
    }, [search, debouncedSearchTerm]);

    if (!data) return <div>Loading...</div>
    if (error) return <div>error</div>

    const deboundSearch = (value: string) => setSearch(value)

    return <React.Fragment>
        <SearchComponents deboundSearch={deboundSearch} />
        <TableComponent data={data} />
    </React.Fragment>
}
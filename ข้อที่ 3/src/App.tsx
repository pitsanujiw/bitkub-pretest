import React from 'react';
import { TableViewPage } from './pages'
import './App.css';

function App() {
  return (
    <React.Fragment>
      <div className="container mx-auto">
        <TableViewPage />
      </div>

    </React.Fragment>
  );
}

export default App;

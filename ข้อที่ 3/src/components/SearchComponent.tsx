import React from 'react'

export const SearchComponents = ({ deboundSearch }: {
    deboundSearch: (value: string) => void
}) => {
    return <React.Fragment>
        <div className="relative mr-6 my-2 w-2/6">
            <input type="search" className="bg-purple-white shadow rounded border-0 p-3 w-100" placeholder="Search..." onChange={(event: React.ChangeEvent<HTMLInputElement>) => deboundSearch(event?.target?.value)} />
            <div className="absolute pin-r pin-t mt-3 mr-4 text-purple-lighter">
            </div>
        </div>
    </React.Fragment>
}
import React from 'react';
import { UserInterface } from '../interface';

export const TableComponent = ({ data }: {
    data: UserInterface[]
}) => {

    return (
        <React.Fragment>
            <div className="flex flex-col">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table className="min-w-full divide-y divide-gray-200">
                                <thead className="bg-gray-50">
                                    <tr>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >Name</th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >Address</th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >Company</th>
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >Phone</th>
                                        <th
                                            scope="col"
                                            className="relative px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                        >Website</th>
                                    </tr>
                                </thead>
                                {data.length > 0 ?
                                    <tbody className="bg-white divide-y divide-gray-200">
                                        {data.map((item, index) =>
                                            <tr key={index}>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <div className="flex items-center">
                                                        <div className="ml-4">
                                                            <div className="text-sm font-medium text-gray-900">
                                                                {item.name}
                                                            </div>
                                                            <div className="text-sm text-gray-500">
                                                                {item.email}
                                                            </div>
                                                            <div className="text-sm text-gray-500">
                                                                {item.username}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <div className="text-sm text-gray-900">
                                                        {item.address.city}
                                                    </div>
                                                    <div className="text-sm text-gray-500">
                                                        {item.address.street}
                                                        <br />
                                                        {item.address.suite}
                                                        <br />
                                                        {item.address.zipcode}
                                                        <br />
                                                    </div>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap">
                                                    <div className="text-sm text-gray-900">
                                                        {item.company.name}
                                                    </div>
                                                    <div className="text-sm text-gray-500">
                                                        {item.company.catchPhrase}
                                                        <br />
                                                        {item.company.bs}
                                                    </div>
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {item.phone}
                                                </td>
                                                <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                    {item.website}
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                    : 'Not Found'}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment >
    );
}
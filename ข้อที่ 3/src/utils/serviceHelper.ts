import axios, { AxiosRequestConfig } from 'axios';
import { UserInterface } from '../interface';
import { URL } from '../constants'
export const fetcher = async (url: string) => await ServiceHelper.requestData({ url })

export enum HTTPMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
  PUT = 'PUT',
}

export class ServiceHelper {
  public static getBaseUrl(): string {

    return URL;
  }

  public static async requestData({ method = HTTPMethod.GET, url = '', headers, data }: AxiosRequestConfig) {
    const requestUrl = `${this.getBaseUrl()}${url}`;
    const options = {
      method,
      headers,
      data,
    };

    try {
      const { data } = await axios(requestUrl, options);
      return data as UserInterface[];
    } catch (e) {
      console.error(e);
      throw new Error(e);
    }
  }
}
